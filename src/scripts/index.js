// cookies
class CookieConsent {
	constructor({ popupSelector, btnSelector, activeClass = '' } = {}) {
		this.popupSelector = document.querySelector(popupSelector)
		this.btnSelector = document.querySelector(btnSelector)
		this.activeClass = activeClass
		this.consentPropertyType = 'site_consent'
	}

	getItem = (key) => {
		const cookies = document.cookie
			.split(';')
			.map((cookie) => cookie.split('='))
			.reduce((acc, [key, value]) => ({ ...acc, [key.trim()]: value }), {})

		return cookies[key]
	}

	setItem = (key, value) => {
		document.cookie = `${key}=${value};expires=Sun, 26 Jun 3567 06:23:41 GMT`
	}

	hasConsented = () => {
		if (this.getItem(this.consentPropertyType) === 'true') {
			return true
		}
		return false
	}

	changeStatus = (prop) => {
		this.setItem(this.consentPropertyType, prop)
	}

	bindTrigger = () => {
		this.btnSelector.addEventListener('click', (e) => {
			e.preventDefault()
			this.popupSelector.classList.remove(this.activeClass)
		})
	}

	init = () => {
		try {
			if (!this.hasConsented()) {
				this.popupSelector.classList.add(this.activeClass)
				this.changeStatus(true)
			}

			this.bindTrigger()
		} catch (e) {
			console.error('Not all data has been transmitted')
		}
	}
}

const cookies = new CookieConsent({
	activeClass: 'cookies__popup_active',
	popupSelector: '.cookies__popup',
	btnSelector: '.cookies__popup .btn__primary'
})

setTimeout(() => {
	cookies.init()
}, 3000)

// header
const shortHeader = document.querySelector('.header__short')
const hamburger = shortHeader.querySelector('.header__short_hamburger')

hamburger.addEventListener('click', () => {
	const navIsActive = shortHeader.classList.contains('active')

	if (navIsActive) {
		shortHeader.classList.remove('active')
		document.body.classList.remove('hidden')
	} else {
		shortHeader.classList.add('active')
		document.body.classList.add('hidden')
	}
})

// accordions
const accordions = document.querySelectorAll('.directions__direction')

function openAccordion(elem) {
	elem.classList.add('active')
	elem.style.maxHeight = `${elem.scrollHeight + 55}px`
}

function closeAccordion(elem) {
	elem.classList.remove('active')
	elem.style.maxHeight = null
}

accordions.forEach((acc) => {
	acc.addEventListener('click', (e) => {
		const elemIsActive = acc.classList.contains('active')
		const prevIsActive = document.querySelector('.directions__direction.active')

		if (prevIsActive) {
			closeAccordion(prevIsActive)
		}

		if (elemIsActive && !e.target.closest('.direction__description .btn__secondary')) {
			closeAccordion(acc)
		} else {
			openAccordion(acc)
		}
	})
})

// interactive map
const map = document.querySelector('.geography__content')
const pins = map.querySelectorAll('.geography__pin')

function openMapBox(elem) {
	elem.classList.add('active')
	if (window.innerWidth < 768) {
		document.body.classList.add('hidden')
	}
}

function closeMapBox(elem) {
	elem.classList.remove('active')
	document.body.classList.remove('hidden')
}

map.scrollLeft = (map.scrollWidth - window.innerWidth) / 2

pins.forEach((pin) => {
	pin.addEventListener('click', () => {
		const pinIsActive = pin.classList.contains('active')

		if (pinIsActive) {
			closeMapBox(pin)
		} else {
			openMapBox(pin)

			document.body.addEventListener('mouseup', (e) => {
				if (!e.target.closest('.geography__pin_mapbox')) {
					closeMapBox(pin)
				}
			})
		}
	})
})

// animations
function onEntry(entry) {
	entry.forEach((change) => {
		if (change.isIntersecting) {
			change.target.classList.add('show')
			setTimeout(() => {
				change.target.classList.remove('animation__fade_in')
				change.target.classList.remove('show')
			}, 2000)
		}
	})
}

const options = {
	threshold: [0.5]
}
const observer = new IntersectionObserver(onEntry, options)
const elements = document.querySelectorAll('.animation__fade_in')

elements.forEach((elem) => {
	observer.observe(elem)
})
